package ru.egprojects.anianime_release.api;

public class Api {

    private static Api sInstance;
    private StringBuilder mUrl;
    private DataLoader mDataLoader;

    public static Api get() {
        if (sInstance == null) {
            sInstance = new Api();
        }

        return sInstance;
    }

    public Api loadAnimes(int page) {
        mUrl = new StringBuilder(ApiConstants.Anidub.URL)
                .append(ApiConstants.Anidub.LOAD_ANIMES)
                .append(page);

        return this;
    }

    public Api doSearch(String query, int page) {
        mUrl = new StringBuilder(ApiConstants.Anidub.URL)
                .append("/")
                .append(ApiConstants.Anidub.DO_SEARCH)
                .append(query.replaceAll(" ", "+"))
                .append("&search_start=")
                .append(page);

        return this;
    }

    public Api loadAnime(String url) {
        mUrl = new StringBuilder(url);
        return this;
    }

    public Api loadScreenshot(String embedUrl) {
        mUrl = new StringBuilder(embedUrl);
        return this;
    }

    public Api loadVideo(String embedUrl) {
        mUrl = new StringBuilder(embedUrl);
        return this;
    }

    public void into(DataLoader.OnDataLoadedListener target) {
        cancel();
        mDataLoader = new DataLoader(target);
        mDataLoader.execute(mUrl.toString());
    }

    public void cancel() {
        if (mDataLoader != null) {
            mDataLoader.cancel(true);
        }
    }
}
