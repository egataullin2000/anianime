package ru.egprojects.anianime_release.api;

public class ApiConstants {

    private static final String PROTOCOL = "https://";

    private static abstract class AnimeService {
        static final String DO_SEARCH = "index.php?do=search&subaction=search&search_start=1&full_search=0&result_from=1&story=";
    }

    public static class Anidub extends AnimeService {
        public static final String TITLE = "AniDUB";
        public static final String DOMAIN = "online.anidub.com";
        static final String URL = PROTOCOL + DOMAIN;
        static final String LOAD_ANIMES = "/page/";
        //static final String LOGIN = "?login_name=gataullin2000&login_password=3cc3j136gc8&login=submit";
    }

    public static class Stormo {
        public static final String TITLE = "Stormo";
        static final String DOMAIN = "https://www.stormo.tv/";
        static final String LOAD_SCREENSHOT = "https://www.stormo.tv/contents/videos_screenshots/";
        static final String LOAD_VIDEO = "https://www.stormo.tv/get_file";
    }

    public static class Sibnet {
        public static final String TITLE = "Sibnet";
        static final String DOMAIN = "https://video.sibnet.ru/";
        static final String LOAD_SCREENSHOT = "http://video.sibnet.ru/upload/cover/video_";
    }

    public static class StreamGuard {
        public static final String TITLE = "StreamGuard";
        static final String DOMAIN = "https://streamguard.cc";
    }

}
