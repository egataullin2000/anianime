package ru.egprojects.anianime_release.api;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ru.egprojects.anianime_release.api.ApiConstants.Anidub;
import ru.egprojects.anianime_release.models.AnimeFull;
import ru.egprojects.anianime_release.models.AnimePreview;

public class DataLoader extends AsyncTask<String, String, String> {

    private OnDataLoadedListener mListener;
    private Object mData;

    DataLoader(OnDataLoadedListener onDataLoadedListener) {
        mListener = onDataLoadedListener;
    }

    @Override
    public String doInBackground(String[] params) {
        String url = params[0];
        try {
            if (mListener instanceof OnImgUrlLoadedListener && (url.contains(ApiConstants.Stormo.DOMAIN) || url.contains(ApiConstants.Sibnet.DOMAIN))) {
                mData = loadScreenshotUrl(url);
            } else if (mListener instanceof OnAnimesLoadedListener) {
                mData = loadAnimes(url);
            } else if (mListener instanceof OnAnimeLoadedListener) {
                mData = loadAnime(url);
            } else if (mListener instanceof OnVideoUrlLoaded) {
                mData = loadVideoUrl(url);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<AnimePreview> loadAnimes(String url) throws IOException {
        if (url.contains(Anidub.DOMAIN)) {
            return AnidubParser.parseAnimes(url);
        } else if (url.contains(ApiConstants.Animaunt.DOMAIN)) {
            return AnimauntParser.parseAnimes(url);
        }

        return null;
    }

    private AnimeFull loadAnime(String url) throws IOException {
        if (url.contains(Anidub.DOMAIN)) {
            return AnidubParser.parseAnime(url);
        } else if (url.contains(ApiConstants.Animaunt.DOMAIN)) {
            return AnimauntParser.parseAnime(url);
        }

        return null;
    }

    private String loadScreenshotUrl(String embedUrl) throws IOException {
        if (embedUrl.contains(ApiConstants.Stormo.DOMAIN)) {
            return StormoParser.parseScreenshotUrl(embedUrl);
        } else if (embedUrl.contains(ApiConstants.Sibnet.DOMAIN)) {
            return SibnetParser.parseScreenshotUrl(embedUrl);
        } else if (embedUrl.contains(ApiConstants.StreamGuard.DOMAIN)) {
            return StreamGuardParser.parseScreenshotUrl(embedUrl);
        }

        return "";
    }

    private String loadVideoUrl(String embedUrl) throws IOException {
        if (embedUrl.contains(ApiConstants.Stormo.DOMAIN)) {
            return StormoParser.parseVideoUrl(embedUrl);
        } else if (embedUrl.contains(ApiConstants.Sibnet.DOMAIN)) {
            return SibnetParser.parseVideoUrl(embedUrl);
        } else if (embedUrl.contains(ApiConstants.StreamGuard.DOMAIN)) {
            return StreamGuardParser.parseVideoUrl(embedUrl);
        }

        return embedUrl;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (mListener == null) {
            return;
        }

        if (mListener instanceof OnAnimesLoadedListener) {
            ((OnAnimesLoadedListener) mListener).onAnimesLoaded((List<AnimePreview>) mData);
        } else if (mListener instanceof OnAnimeLoadedListener && mData instanceof AnimeFull) {
            ((OnAnimeLoadedListener) mListener).onAnimeLoaded((AnimeFull) mData);
        } else if (mListener instanceof OnImgUrlLoadedListener) {
            ((OnImgUrlLoadedListener) mListener).onImageUrlLoaded((String) mData);
        } else if (mListener instanceof OnVideoUrlLoaded) {
            ((OnVideoUrlLoaded) mListener).onVideoUrlLoaded((String) mData);
        }
    }

    interface OnDataLoadedListener {
    }

    @FunctionalInterface
    public interface OnAnimesLoadedListener extends OnDataLoadedListener {
        void onAnimesLoaded(List<AnimePreview> animes);
    }

    @FunctionalInterface
    public interface OnAnimeLoadedListener extends OnDataLoadedListener {
        void onAnimeLoaded(AnimeFull anime);
    }

    @FunctionalInterface
    public interface OnImgUrlLoadedListener extends OnDataLoadedListener {
        void onImageUrlLoaded(String url);
    }

    @FunctionalInterface
    public interface OnVideoUrlLoaded extends OnDataLoadedListener {
        void onVideoUrlLoaded(String url);
    }

    private static class AnidubParser {

        private static List<AnimePreview> parseAnimes(String url) throws IOException {
            boolean isSearch = url.contains(Anidub.DO_SEARCH);
            Element content = Jsoup.connect(url).get().select("div#dle-content").get(0);
            Elements newsTitles;
            Elements newsShorts = content.select("div.news_short");
            if (isSearch) {
                newsTitles = content.select("div.title");
                newsShorts.remove(0);
            } else {
                newsTitles = content.select("h2.title");
            }
            List<AnimePreview> animes = new ArrayList<>(newsTitles.size());
            for (int i = 0; i < newsTitles.size(); i++) {
                Element element = newsTitles.get(i).child(0);
                AnimePreview anime = new AnimePreview(element.attr("href"));
                Elements elements = newsShorts.get(i).select("li:contains(Жанр)");
                StringBuilder s;
                if (elements.size() > 0 && elements.get(0).child(1).childNodeSize() > 1) {
                    elements = elements.get(0).child(1).getAllElements();
                    s = new StringBuilder(elements.get(1).ownText());
                    for (int j = 2; j < elements.size(); j++) {
                        s.append(", ").append(elements.get(j).ownText());
                    }
                    anime.setGenres(s.toString());
                } else {
                    continue;
                }
                s = new StringBuilder(element.wholeText());
                int index = s.indexOf(" /");
                if (index != -1) {
                    s.delete(index, s.length());
                }
                anime.setTitle(s.toString());
                element = newsShorts.get(i).select("div.poster_img").get(0).child(0);
                if (isSearch) {
                    s = new StringBuilder(element.attr("src"));
                } else {
                    s = new StringBuilder(element.child(0).attr("data-original"));
                }
                anime.setImageUrl(s.toString());
                animes.add(anime);
            }

            return animes;
        }

        private static AnimeFull parseAnime(String url) throws IOException {
            AnimeFull anime = new AnimeFull(url);
            Document doc = Jsoup.connect(url).get();
            StringBuilder s = new StringBuilder(doc.select("h1.titlfull").get(0).ownText());
            anime.setEpisodesCount(s.substring(s.indexOf("[") + 1, s.indexOf("]")));
            anime.setTitle(s.substring(0, s.indexOf(" /")));
            anime.setImageUrl(doc.select("img[itemprop]").get(0).attr("src"));
            Element content = doc.select("div.maincont").get(0);
            Element reset = content.select("ul.reset").get(0);
            Element element = doc.select("div.rate_view").last().child(1);
            if (element.ownText().contains("%")) {
                anime.setRating(element.ownText());
            } else {
                anime.setRating(element.child(0).ownText() + "/5");
            }
            Elements elements = reset.select("li:contains(Год)");
            if (elements.size() > 0) {
                anime.setYear(elements.get(0).child(1).ownText());
            }
            elements = reset.select("li:contains(Страна)");
            if (elements.size() > 0) {
                anime.setCountry(elements.get(0).child(1).child(0).ownText());
            }
            elements = reset.select("li:contains(Режиссер)");
            if (elements.size() > 0) {
                anime.setProducer(elements.get(0).child(1).child(1).ownText());
            }
            elements = reset.select("li:contains(Автор)");
            if (elements.size() > 0) {
                anime.setAuthor(elements.get(0).child(1).child(1).ownText());
            }
            elements = reset.select("li:contains(Озвучивание)");
            int index;
            if (elements.size() > 0) {
                s = new StringBuilder(elements.get(0).wholeText());
                index = s.indexOf("Озвучивание: ");
                if (index != -1) {
                    s.delete(0, "Озвучивание: ".length());
                }
                anime.setDubbers(s.toString());
            }
            elements = reset.select("li:contains(Жанр)").get(0).child(1).getAllElements();
            s = new StringBuilder(elements.get(2).ownText());
            for (int i = 2; i <= (elements.size()) / 2; i++) {
                s.append(", ").append(elements.get(i * 2).ownText());
            }
            anime.setGenres(s.toString());
            element = content.select("b:contains(Описание)").get(0).parent();
            s = new StringBuilder(element.ownText());
            if (s.toString().startsWith(":")) {
                s.delete(0, 2);
            }
            anime.setDescription(s.toString());
            elements = content.select("select#sel3");
            if (elements.size() != 0) {
                elements = elements.get(0).getAllElements();
            }
            String[][] titles = new String[2][];
            String[][] embedUrls = new String[2][];
            if (elements.size() != 0) {
                titles[0] = new String[elements.size() - 1];
                embedUrls[0] = new String[elements.size() - 1];
            } else {
                titles[0] = new String[0];
                embedUrls[0] = new String[0];
            }
            for (int i = 0; i < titles[0].length; i++) {
                s = new StringBuilder(elements.get(i + 1).ownText());
                index = s.indexOf(anime.getTitle());
                if (index != -1) {
                    s.delete(0, anime.getTitle().length() + index + 3);
                }
                index = s.indexOf(anime.getDubbers());
                if (index != -1) {
                    s.delete(index - 1, s.length());
                }
                titles[0][i] = s.toString();
                s = new StringBuilder(elements.get(i + 1).attr("value"));
                s.delete(s.indexOf("|"), s.length());
                embedUrls[0][i] = s.toString();
            }
            elements = content.select("select#sel");
            if (elements.size() != 0) {
                elements = elements.first().getAllElements();
            }
            if (elements.size() != 0) {
                titles[1] = new String[elements.size() - 1];
                embedUrls[1] = new String[elements.size() - 1];
            } else {
                titles[1] = new String[0];
                embedUrls[1] = new String[0];
            }
            for (int i = 0; i < titles[1].length; i++) {
                s = new StringBuilder(elements.get(i + 1).ownText());
                index = s.indexOf(anime.getTitle());
                if (index != -1) {
                    s.delete(0, anime.getTitle().length() + index + 3);
                }
                index = s.indexOf(anime.getDubbers());
                if (index != -1) {
                    s.delete(index - 1, s.length());
                }
                titles[1][i] = s.toString();
                s = new StringBuilder(elements.get(i + 1).attr("value"));
                index = s.indexOf("|");
                if (index != -1) {
                    s.delete(index, s.length());
                }
                index = s.indexOf("\"");
                if (index != -1) {
                    s.delete(index, s.length());
                }
                embedUrls[1][i] = s.toString();
            }
            if (titles[0].length == 0) {
                if (titles[1].length != 0) {
                    titles[0] = titles[1];
                    titles[1] = new String[0];
                } else {
                    titles[0] = new String[1];
                    titles[0][0] = doc.select("div#mcode_block")
                            .first()
                            .child(0)
                            .attr("src")
                            .replace("//", "http://www.");
                }
            }
            anime.setEpisodesTitles(titles);
            anime.setEmbedUrls(embedUrls);

            return anime;
        }

    }

    private static class AnimauntParser {

        private static List<AnimePreview> parseAnimes(String url) throws IOException {
            Elements elements = Jsoup.connect(url).get().select("div.th-in");
            List<AnimePreview> animes = new ArrayList<>(elements.size());
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                AnimePreview anime = new AnimePreview(element.child(0).attr("href"));
                anime.setImageUrl(ApiConstants.Animaunt.URL +
                        element.select("img[src]").attr("src")
                );
                anime.setTitle(element.child(1).child(0).ownText());
                anime.setGenres(
                        element.child(1).child(1).ownText().replace(" /", ",")
                );
                animes.add(anime);
            }

            return animes;
        }

        private static AnimeFull parseAnime(String url) throws IOException {
            Elements content = Jsoup.connect(url).get()
                    .select("div#dle-content").get(0)
                    .children();
            AnimeFull anime = new AnimeFull(url);
            anime.setDescription(content.select("div#fdesc").get(0).wholeText());
            Element element = content.select("div.fleft").get(0).child(0).child(0);
            anime.setTitle(element.attr("alt"));
            StringBuilder s = new StringBuilder("https://")
                    .append(ApiConstants.Animaunt.DOMAIN)
                    .append(element.attr("src"));
            anime.setImageUrl(s.toString());
            element = content.select("div.fser-list").get(0);
            String[][] strings = new String[1][element.children().size()];
            for (int i = 0; i < strings[0].length; i++) {
                strings[0][i] = element.child(i).ownText();
            }
            anime.setEpisodesTitles(strings);
            strings = new String[1][strings[0].length];
            element = content.select("script").get(1);
            s = new StringBuilder(element.data());
            for (int i = 0; i < strings[0].length; i++) {
                s = new StringBuilder(s.substring(s.indexOf("http")));
                strings[0][i] = s.substring(0, s.indexOf("\""))
                        .replaceAll("\\\\", "");
                s = new StringBuilder(s.substring(s.indexOf("/")));
            }
            anime.setEmbedUrls(strings);
            content = content.select("ul.finfo").get(0).children();
            element = content.select("li:contains(Жанр)").get(0);
            s = new StringBuilder(element.child(1).ownText());
            for (int i = 3; i < element.childNodeSize() - 2; i += 2) {
                s.append(", ").append(element.child(i).ownText());
            }
            anime.setGenres(s.toString());
            anime.setYear(content.select("li:contains(Год)").get(0).ownText());
            Elements elements = content.select("li:contains(Эпизоды)");
            if (elements.size() > 0) {
                anime.setEpisodesCount(elements.get(0).ownText());
            }
            anime.setCountry(content.select("li:contains(Страна)").get(0).ownText());
            elements = content.select("li:contains(Режиссёр)");
            if (elements.size() > 0) {
                anime.setProducer(elements.get(0).ownText());
            }
            content = content.select("li:contains(Озвучка)").get(0).children();
            s = new StringBuilder(content.get(1).ownText());
            for (int i = 3; i < content.size(); i += 2) {
                s.append(", ").append(content.get(i).ownText());
            }
            anime.setDubbers(s.toString());

            return anime;
        }

    }

    private static class StormoParser {

        private static String parseScreenshotUrl(String embedUrl) throws IOException {
            Document doc = Jsoup.parse(new URL(embedUrl).openStream(), "UTF-8", embedUrl);
            StringBuilder url = new StringBuilder(doc.child(0).child(1).child(1).data());
            url.delete(0, url.indexOf(ApiConstants.Stormo.LOAD_SCREENSHOT));
            url.delete(url.indexOf("'"), url.length());

            return url.toString();
        }

        private static String parseVideoUrl(String embedUrl) throws IOException {
            Document doc = Jsoup.parse(new URL(embedUrl).openStream(), "UTF-8", embedUrl);
            StringBuilder url = new StringBuilder(doc.child(0).child(1).child(1).data());
            url.delete(0, url.indexOf(ApiConstants.Stormo.LOAD_VIDEO));
            url.delete(url.indexOf("/?embed=true"), url.length());

            return url.toString();
        }

    }

    private static class SibnetParser {

        private static String parseScreenshotUrl(String embedUrl) {
            return ApiConstants.Sibnet.LOAD_SCREENSHOT +
                    embedUrl.substring(embedUrl.indexOf("=") + 1, embedUrl.length()) + "_0.jpg";
        }

        private static String parseVideoUrl(String embedUrl) throws IOException {
            if (embedUrl.contains(".mp4")) {
                return embedUrl;
            }

            String link = Jsoup.connect(embedUrl)
                    .userAgent("Android")
                    .post()
                    .child(0)
                    .child(1)
                    .child(26)
                    .data();
            link = link.substring(link.indexOf("v/"));
            link = link.substring(0, link.indexOf("\""));

            return ApiConstants.Sibnet.DOMAIN + link;
        }

    }

    private static class StreamGuardParser {

        private static String parseScreenshotUrl(String embedUrl) {
            return null;
        }

        private static String parseVideoUrl(String embedUrl) {
            return embedUrl;
        }

    }

}
