package ru.egprojects.anianime_release.player;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.ohoussein.playpause.PlayPauseView;

import ru.egprojects.anianime_release.R;
import ru.egprojects.anianime_release.api.Api;
import ru.egprojects.anianime_release.api.DataLoader;

public class AnimePlayer implements MediaController.MediaPlayerControl, DataLoader.OnVideoUrlLoaded {

    private Context mContext;
    private String mDataSource;
    private String[] mEmbedUrls;
    private int mInitialPosition;
    private SimpleExoPlayer mPlayer;
    private PlayPauseView mPlayPause;
    private Player.EventListener mEventListener;

    public AnimePlayer(Context context, String[] embedUrls) {
        mContext = context;
        mEmbedUrls = embedUrls;
        DefaultTrackSelector trackSelector = new DefaultTrackSelector();
        mPlayer = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector);
        mEventListener = new Player.EventListener() {
            @Override
            public void onPlayerError(ExoPlaybackException e) {
                Toast.makeText(mContext, R.string.error, Toast.LENGTH_SHORT).show();
                ((Activity) mContext).finish();
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING) {
                    mPlayPause.setVisibility(View.INVISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    mPlayPause.setVisibility(View.VISIBLE);
                }
            }
        };
        mPlayer.addListener(mEventListener);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        mPlayer.setAudioAttributes(audioAttributes, true);
    }

    @SuppressWarnings("ConstantConditions")
    public void setPlayerView(PlayerView playerView) {
        playerView.setPlayer(mPlayer);
        if (mContext instanceof Activity) {
            playerView.setControllerVisibilityListener(visibility -> {
                if (visibility == View.VISIBLE) {
                    ((Activity) mContext).getWindow()
                            .clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                } else {
                    ((Activity) mContext).getWindow()
                            .addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            });
        }
        if (mContext instanceof AppCompatActivity) {
            ((AppCompatActivity) mContext).setSupportActionBar(playerView.findViewById(R.id.toolbar));
            ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mPlayPause = playerView.findViewById(R.id.player_play_pause);
        mPlayPause.change(false, false);
        mPlayPause.setOnClickListener(view -> {
            if (isPlaying()) {
                pause();
            } else {
                start();
            }
        });
    }

    public void play(int episode) {
        Api.get().loadVideo(mEmbedUrls[episode]).into(this);
        pause();
    }

    public String getDataSource() {
        return mDataSource;
    }

    public void setDataSource(String source) {
        mDataSource = source;
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mContext, mContext.getString(R.string.app_name));
        ExtractorMediaSource.Factory factory = new ExtractorMediaSource.Factory(dataSourceFactory);
        mPlayer.prepare(factory.createMediaSource(Uri.parse(mDataSource)));
        mPlayer.seekTo(mInitialPosition);
        mInitialPosition = 0;
        start();
    }

    public void release() {
        if (mPlayer != null) {
            mPlayer.removeListener(mEventListener);
            mPlayer.release();
            mPlayer = null;
            mContext = null;
            mDataSource = null;
            mEventListener = null;
        }
    }

    @Override
    public void onVideoUrlLoaded(String url) {
        setDataSource(url);
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getBufferPercentage() {
        return mPlayer.getBufferedPercentage();
    }

    @Override
    public int getCurrentPosition() {
        return (int) mPlayer.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return (int) mPlayer.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return mPlayer.getPlayWhenReady();
    }

    @Override
    public void start() {
        mPlayPause.toggle();
        mPlayer.setPlayWhenReady(true);
    }

    @Override
    public void pause() {
        mPlayPause.toggle();
        mPlayer.setPlayWhenReady(false);
    }

    @Override
    public void seekTo(int timeMillis) {
        mPlayer.seekTo(timeMillis);
    }

}
