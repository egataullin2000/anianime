package ru.egprojects.anianime_release;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Tools {

    public static boolean hasInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();

        return wifiInfo != null && wifiInfo.isConnected();
    }

    public static String getStringForTime(long msec) {
        long seconds = msec / 1000;
        long minutes = TimeUnit.SECONDS.toMinutes(seconds);
        seconds -= TimeUnit.MINUTES.toSeconds(minutes);
        String time;
        Locale locale = Locale.getDefault();
        if (minutes >= 60) {
            long hours = TimeUnit.MINUTES.toHours(minutes);
            minutes -= TimeUnit.HOURS.toMinutes(hours);
            time = String.format(locale, "%02d:%02d:%02d", hours, minutes, seconds);
        } else {
            time = String.format(locale, "%02d:%02d", minutes, seconds);
        }

        return time;
    }

}
