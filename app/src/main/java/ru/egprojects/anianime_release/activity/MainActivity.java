package ru.egprojects.anianime_release.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import ru.egprojects.anianime_release.R;
import ru.egprojects.anianime_release.adapter.AdapterWithLoadingData;
import ru.egprojects.anianime_release.fragment.AnimeListFragment;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, AdapterWithLoadingData.OnDataUpdatedListener {

    private MaterialSearchView mSearchView;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setTitle(item.getTitle());

        mSearchView.closeSearch();
        mRefreshLayout.setEnabled(false);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.content, new AnimeListFragment())
                .commit();

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(R.string.title_home);

        initRefreshLayout();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, new AnimeListFragment())
                .commit();
    }

    private void initRefreshLayout() {
        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(() -> {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if (fragment instanceof AnimeListFragment) {
                ((AnimeListFragment) fragment).updateUI();
            }
        });
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        mSearchView = findViewById(R.id.search_view);
        mSearchView.setMenuItem(searchItem);
        mSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchView.clearFocus();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
                if (fragment instanceof AnimeListFragment) {
                    ((AnimeListFragment) fragment).doSearch(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        mSearchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
                if (fragment instanceof AnimeListFragment) {
                    ((AnimeListFragment) fragment).cancelSearch();
                }
            }
        });

        return true;
    }

    @Override
    public void onDataUpdated() {
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRefreshLayout = null;
        mSearchView = null;
    }

}
