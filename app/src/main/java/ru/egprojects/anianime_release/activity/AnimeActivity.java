package ru.egprojects.anianime_release.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import ru.egprojects.anianime_release.view.FieldView;
import ru.egprojects.anianime_release.R;
import ru.egprojects.anianime_release.api.Api;
import ru.egprojects.anianime_release.api.ApiConstants;
import ru.egprojects.anianime_release.api.DataLoader;
import ru.egprojects.anianime_release.models.AnimeFull;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AnimeActivity extends AppCompatActivity implements DataLoader.OnAnimeLoadedListener,
        DataLoader.OnImgUrlLoadedListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String EXTRA_ANIME_URL = "ru.egprojects.anianime_release.anime_url";

    private AnimeFull mAnime;
    private SwipeRefreshLayout mRefreshLayout;
    private int mVideoHost;
    private int mEpisode;
    private ImageView mScreenshotView;

    public static void start(Context context, String animeUrl) {
        Intent intent = new Intent(context, AnimeActivity.class)
                .putExtra(EXTRA_ANIME_URL, animeUrl);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anime);

        onRefresh();
        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {
        Api.get().loadAnime(getIntent().getStringExtra(EXTRA_ANIME_URL)).into(this);
    }

    @Override
    public void onAnimeLoaded(AnimeFull anime) {
        mAnime = anime;
        if (mAnime.getEmbedUrls()[mVideoHost].length == 0) {
            mVideoHost++;
        }
        mRefreshLayout.setRefreshing(false);
        findViewById(R.id.appbar).setVisibility(View.VISIBLE);
        findViewById(R.id.nested_scroll_view).setVisibility(View.VISIBLE);
        findViewById(R.id.fab).setVisibility(View.VISIBLE);
        initToolbar();
        initInformation();
        loadImage();
        initSpinners();
        initButtons();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mAnime.getTitle());
    }

    private void initInformation() {
        ((ExpandableTextView) findViewById(R.id.description)).setText(mAnime.getDescription());
        ((FieldView) findViewById(R.id.year)).setValue(mAnime.getYear());
        ((FieldView) findViewById(R.id.genre)).setValue(mAnime.getGenres());
        ((FieldView) findViewById(R.id.country)).setValue(mAnime.getCountry());
        ((FieldView) findViewById(R.id.dubbers)).setValue(mAnime.getDubbers());
        ((FieldView) findViewById(R.id.rating)).setValue(mAnime.getRating());
        ((FieldView) findViewById(R.id.producer)).setValue(mAnime.getProducer());
        ((FieldView) findViewById(R.id.author)).setValue(mAnime.getAuthor());
        ((FieldView) findViewById(R.id.episodes_count)).setValue(mAnime.getEpisodesCount());
    }

    private void loadImage() {
        ImageView imageView = findViewById(R.id.image);
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .override(imageView.getWidth(), imageView.getHeight())
                .priority(Priority.HIGH);
        Glide.with(getApplicationContext())
                .load(mAnime.getImageUrl())
                .apply(options)
                .transition(withCrossFade())
                .into(imageView);
    }

    private void initSpinners() {
        if (mAnime.getEpisodesTitles().length != 1 && mAnime.getEpisodesTitles()[0].length != 0 && mAnime.getEpisodesTitles()[1].length != 0) {
            String[] videoHosts = new String[2];
            for (int i = 0; i < videoHosts.length; i++) {
                String url = mAnime.getEmbedUrls()[i][0];
                if (url.contains(ApiConstants.Stormo.TITLE.toLowerCase())) {
                    videoHosts[i] = ApiConstants.Stormo.TITLE;
                } else if (url.contains(ApiConstants.Sibnet.TITLE.toLowerCase())) {
                    videoHosts[i] = ApiConstants.Sibnet.TITLE;
                } else if (url.contains(ApiConstants.StreamGuard.TITLE.toLowerCase())) {
                    videoHosts[i] = ApiConstants.StreamGuard.TITLE;
                }
            }
            Spinner videoHostSpinner = findViewById(R.id.video_service_spinner);
            ArrayAdapter<String> videoHostAdapter = new ArrayAdapter<>(
                    this,
                    R.layout.activity_anime_spinner_dropdown_item,
                    R.id.text1,
                    videoHosts
            );
            videoHostSpinner.setAdapter(videoHostAdapter);
            videoHostSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    mVideoHost = i;
                    Spinner episodeSpinner = findViewById(R.id.episode_spinner);
                    String[] episodesTitles = mAnime.getEpisodesTitles()[mVideoHost];
                    ArrayAdapter<String> episodeAdapter = new ArrayAdapter<>(
                            AnimeActivity.this,
                            R.layout.activity_anime_spinner_dropdown_item,
                            R.id.text1,
                            episodesTitles
                    );
                    episodeSpinner.setAdapter(episodeAdapter);
                    loadScreenshot();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            videoHostSpinner.setVisibility(View.VISIBLE);
        }
        if (mAnime.getEpisodesTitles()[0].length > 1) {
            Spinner episodeSpinner = findViewById(R.id.episode_spinner);
            episodeSpinner.setVisibility(View.VISIBLE);
            String[] episodesTitles = mAnime.getEpisodesTitles()[mVideoHost];
            ArrayAdapter<String> episodeAdapter = new ArrayAdapter<>(
                    this,
                    R.layout.activity_anime_spinner_dropdown_item,
                    R.id.text1,
                    episodesTitles
            );
            episodeSpinner.setAdapter(episodeAdapter);
            episodeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mEpisode != position) {
                        mEpisode = position;
                        loadScreenshot();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void loadScreenshot() {
        Api.get()
                .loadScreenshot(mAnime.getEmbedUrls()[mVideoHost][mEpisode])
                .into(this);
    }

    private void initButtons() {
        mScreenshotView = findViewById(R.id.screenshot_container);
        loadScreenshot();
        mScreenshotView.setOnClickListener(
                view -> PlayerActivity.start(this, mAnime, mVideoHost, mEpisode)
        );
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(
                view -> PlayerActivity.start(this, mAnime, 0, 0)
        );
        fab.hide();
    }

    @Override
    public void onImageUrlLoaded(String url) {
        Glide.with(getApplicationContext())
                .load(url)
                .into(mScreenshotView);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
