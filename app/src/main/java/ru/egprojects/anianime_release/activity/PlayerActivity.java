package ru.egprojects.anianime_release.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import ru.egprojects.anianime_release.R;
import ru.egprojects.anianime_release.api.Api;
import ru.egprojects.anianime_release.api.DataLoader;
import ru.egprojects.anianime_release.models.AnimeFull;
import ru.egprojects.anianime_release.player.AnimePlayer;

public class PlayerActivity extends AppCompatActivity implements DataLoader.OnAnimeLoadedListener {

    private static final String EXTRA_ANIME = "ru.egprojects.anianime_release.anime";
    private static final String EXTRA_VIDEO_HOST = "ru.egprojects.anianime_release.video_host";
    private static final String EXTRA_EPISODE = "ru.egprojects.anianime_release.episode";
    private static final String EXTRA_VIDEO_PATH = "ru.egprojects.anianime_release.video_path";

    private AnimeFull mAnime;
    private boolean mWasPlaying;
    private AnimePlayer mPlayer;
    private int mVideoHost, mEpisode;

    public static void start(Context context, AnimeFull anime, int videoHost, int episode) {
        Intent intent = new Intent(context, PlayerActivity.class)
                .putExtra(EXTRA_ANIME, new Gson().toJson(anime))
                .putExtra(EXTRA_VIDEO_HOST, videoHost)
                .putExtra(EXTRA_EPISODE, episode);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        // hiding navigation bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        loadAnime();
    }

    private void loadAnime() {
        Intent intent = getIntent();
        String animeString = intent.getStringExtra(EXTRA_ANIME);
        try {
            AnimeFull anime = new Gson().fromJson(animeString, AnimeFull.class);
            onAnimeLoaded(anime);
        } catch (JsonSyntaxException e) {
            Api.get().loadAnime(animeString).into(this);
        }
    }

    @Override
    public void onAnimeLoaded(AnimeFull anime) {
        mAnime = anime;
        setTitle(mAnime.getTitle());
        Intent intent = getIntent();
        mVideoHost = intent.getIntExtra(EXTRA_VIDEO_HOST, 0);
        mEpisode = intent.getIntExtra(EXTRA_EPISODE, 0);
        mPlayer = new AnimePlayer(this, mAnime.getEmbedUrls()[mVideoHost]);
        String videoPath = intent.getStringExtra(EXTRA_VIDEO_PATH);
        if (videoPath != null) {
            mPlayer.setDataSource(videoPath);
        }
        mPlayer.setPlayerView(findViewById(R.id.display));
        mPlayer.play(mEpisode);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mWasPlaying) {
            mPlayer.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPlayer != null) {
            mWasPlaying = mPlayer.isPlaying();
            mPlayer.pause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            mPlayer.release();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
