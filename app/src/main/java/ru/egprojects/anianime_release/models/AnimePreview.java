package ru.egprojects.anianime_release.models;

public class AnimePreview {

    private String mUrl;
    private String mTitle;
    private String mImageUrl;
    private String mGenres;

    public AnimePreview(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String url) {
        mImageUrl = url;
    }

    public String getGenres() {
        return mGenres;
    }

    public void setGenres(String genres) {
        mGenres = genres;
    }

}
