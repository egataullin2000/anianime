package ru.egprojects.anianime_release.models;

import java.util.Arrays;
import java.util.Collections;

public class AnimeFull extends AnimePreview {

    private String mDescription;
    private String mRating;
    private String mYear;
    private String mCountry;
    private String mDubbers;
    private String mProducer;
    private String mAuthor;
    private String mEpisodesCount;
    private String[][] mEpisodesTitles; // mEpisodesTitles[video_host][episode]
    private String[][] mEmbedUrls; // mEmbedUrls[video_host][episode]

    public AnimeFull(String url) {
        super(url);
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getRating() {
        return mRating;
    }

    public void setRating(String rating) {
        mRating = rating;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        mYear = year;
    }

    public String getDubbers() {
        return mDubbers;
    }

    public void setDubbers(String dubbers) {
        mDubbers = dubbers;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getProducer() {
        return mProducer;
    }

    public void setProducer(String producer) {
        mProducer = producer;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String originalAuthor) {
        mAuthor = originalAuthor;
    }

    public String getEpisodesCount() {
        return mEpisodesCount;
    }

    public void setEpisodesCount(String count) {
        mEpisodesCount = count;
    }

    public String[][] getEpisodesTitles() {
        return mEpisodesTitles;
    }

    public void setEpisodesTitles(String[][] titles) {
        mEpisodesTitles = titles;
    }

    public String[][] getEmbedUrls() {
        return mEmbedUrls;
    }

    public void setEmbedUrls(String[][] embedUrls) {
        mEmbedUrls = embedUrls;
        sortEpisodes();
    }

    private void sortEpisodes() {
        if (mEpisodesTitles[0].length > 1) {
            String title = mEpisodesTitles[0][0];
            String number1;
            String number2;
            short i = 0;
            while (!title.contains("серия") && i < mEpisodesTitles.length) {
                i++;
                title = mEpisodesTitles[0][i];
            }
            number1 = title.substring(0, title.indexOf(" "));
            i = (short) (mEpisodesTitles[0].length - 1);
            title = mEpisodesTitles[0][i];
            while (!title.contains("серия")) {
                i--;
                title = mEpisodesTitles[0][i];
            }
            number2 = title.substring(0, title.indexOf(" "));
            if (number1.length() > 0 && number2.length() > 0) {
                short n1 = Short.parseShort(number1);
                short n2 = Short.parseShort(number2);
                if (n1 > n2) {
                    for (int k = 0; k < mEpisodesTitles.length; k++) {
                        Collections.reverse(Arrays.asList(mEpisodesTitles[k]));
                        Collections.reverse(Arrays.asList(mEmbedUrls[k]));
                    }
                }
            }
        }
    }

}
