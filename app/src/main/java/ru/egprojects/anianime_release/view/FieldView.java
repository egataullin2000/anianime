package ru.egprojects.anianime_release.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.card.MaterialCardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.egprojects.anianime_release.R;

public class FieldView extends MaterialCardView {

    public FieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.field_view, this, true);
        TypedArray attrArray = context.obtainStyledAttributes(
                attrs, R.styleable.FieldView, 0, 0
        );
        ((TextView) findViewById(R.id.field_view_title))
                .setText(attrArray.getString(R.styleable.FieldView_title));
        attrArray.recycle();
        setUseCompatPadding(true);
        float pixels = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                2,
                getResources().getDisplayMetrics()
        );
        setCardElevation(pixels);
        setRadius(0);
    }

    public FieldView(Context context) {
        this(context, null);
    }

    public void setValue(String value) {
        if (value == null) {
            ((ViewGroup) getParent()).removeView(this);
        } else {
            ((TextView) findViewById(R.id.field_view_value)).setText(value);
        }
    }

}
