package ru.egprojects.anianime_release.view;

import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.TextView;

import java.lang.reflect.Field;

import ru.egprojects.anianime_release.R;

public class SnackBar {

    public static Snackbar make(Context context, View anchorView, int resId, int length) {
        Resources resources = context.getResources();
        Snackbar snackbar = Snackbar
                .make(anchorView, resId, length)
                .setActionTextColor(resources.getColor(R.color.colorPrimary));
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        int imageResId;
        if (resId == R.string.bookmark_was_deleted || resId == R.string.download_was_deleted) {
            imageResId = R.drawable.ic_done;
        } else {
            imageResId = R.drawable.ic_done_all;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(imageResId, 0, 0, 0);
        textView.setCompoundDrawablePadding(resources.getDimensionPixelOffset(R.dimen.search_icon_padding));
        reestablishAnimation(snackbar);

        return snackbar;
    }

    // On some phones snackbar shows without animation. This method solves this problem
    private static void reestablishAnimation(Snackbar snackbar) {
        try {
            Field accessibilityManagerField = BaseTransientBottomBar.class
                    .getDeclaredField("mAccessibilityManager");
            accessibilityManagerField.setAccessible(true);
            AccessibilityManager accessibilityManager =
                    (AccessibilityManager) accessibilityManagerField.get(snackbar);
            Field isEnabledField = AccessibilityManager.class.getDeclaredField("mIsEnabled");
            isEnabledField.setAccessible(true);
            isEnabledField.setBoolean(accessibilityManager, false);
            accessibilityManagerField.set(snackbar, accessibilityManager);
        } catch (Exception e) {
            Log.d("Snackbar", "Reflection error: " + e.toString());
        }
    }

}
