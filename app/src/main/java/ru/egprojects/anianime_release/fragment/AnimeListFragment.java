package ru.egprojects.anianime_release.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import ru.egprojects.anianime_release.R;
import ru.egprojects.anianime_release.activity.AnimeActivity;
import ru.egprojects.anianime_release.adapter.AdapterWithLoadingData;
import ru.egprojects.anianime_release.api.Api;
import ru.egprojects.anianime_release.api.DataLoader;
import ru.egprojects.anianime_release.models.AnimePreview;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AnimeListFragment extends Fragment implements DataLoader.OnAnimesLoadedListener {

    private Api mApi;
    private AdapterWithLoadingData<AnimePreview> mAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);
        mApi = Api.get();
        RecyclerView recycler = view.findViewById(R.id.recycler_view);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new AdapterWithLoadingData<>(getActivity(), inflater);
        mAdapter.setOnLoadListener(page -> mApi.loadAnimes(page).into(this));
        mAdapter.setOnSearchListener((page, query) ->
                mApi.doSearch(query, page).into(this)
        );
        mAdapter.setOnBindListener((anime, pos, imageView, titleView, genreView) -> {
            final DiskCacheStrategy cacheStrategyNone = DiskCacheStrategy.NONE;
            final DiskCacheStrategy cacheStrategyResource = DiskCacheStrategy.RESOURCE;
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(pos >= 15 ? cacheStrategyNone : cacheStrategyResource)
                    .override(imageView.getWidth(), imageView.getHeight())
                    .transform(new RoundedCorners(8));
            Glide.with(this)
                    .load(anime.getImageUrl())
                    .apply(options)
                    .transition(withCrossFade())
                    .into(imageView);
            titleView.setText(anime.getTitle());
            genreView.setText(anime.getGenres());
        });
        mAdapter.setOnClickListener(anime -> AnimeActivity.start(getActivity(), anime.getUrl()));
        recycler.setAdapter(mAdapter);

        return view;
    }

    public void updateUI() {
        mAdapter.updateData(false);
    }

    public void doSearch(String query) {
        mAdapter.search(query);
    }

    public void cancelSearch() {
        mAdapter.cancelSearch();
    }

    @Override
    public void onAnimesLoaded(List<AnimePreview> animes) {
        if (mAdapter.needUpdate()) {
            mAdapter.setData(animes);
        } else {
            mAdapter.addData(animes);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApi.cancel();
        mApi = null;
        mAdapter = null;
    }

}
