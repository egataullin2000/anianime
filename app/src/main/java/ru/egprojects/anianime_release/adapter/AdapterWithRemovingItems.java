package ru.egprojects.anianime_release.adapter;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import ru.egprojects.anianime_release.R;

public class AdapterWithRemovingItems<I> extends AdapterWithLoadingData<I> {

    private static final int CHECKBOX_ANIMATION_DURATION = 125;

    private Fragment mParent;
    private List<Boolean> mAreItemsChecked;
    private boolean mIsCheckingModeEnabled;
    private OnRemoveListener<I> mOnRemoveListener;

    public AdapterWithRemovingItems(Fragment fragment, LayoutInflater inflater) {
        super(fragment.getActivity(), inflater);
        mParent = fragment;
    }

    public boolean isCheckingModeEnabled() {
        return mIsCheckingModeEnabled;
    }

    public void setCheckingModeEnabled(boolean isEnabled) {
        mIsCheckingModeEnabled = isEnabled;
        if (!mIsCheckingModeEnabled) {
            mAreItemsChecked = new ArrayList<>(mData.size());
            for (int i = 0; i < mData.size(); i++) {
                mAreItemsChecked.add(false);
            }
        }
        notifyItemRangeChanged(0, mData.size(), new Object());
    }

    private void setItemChecked(int position, boolean isChecked) {
        mAreItemsChecked.set(position, isChecked);
    }

    public void setAllItemsChecked() {
        for (int i = 0; i < mAreItemsChecked.size(); i++) {
            setItemChecked(i, true);
        }
    }

    public I getCheckedItem() {
        for (int i = 0; i < mAreItemsChecked.size(); i++) {
            if (mAreItemsChecked.get(i)) {
                return mData.get(i);
            }
        }

        return null;
    }

    private void insertItem(int position, I item) {
        mData.add(position, item);
        mAreItemsChecked.add(position, false);
        notifyItemInserted(position);
    }

    public void removeItems() {
        for (int i = 0; i < mData.size(); i++) {
            if (mAreItemsChecked.get(i)) {
                if (mOnRemoveListener != null) {
                    mOnRemoveListener.onRemove(mData.get(i));
                }
                mData.remove(i);
                mAreItemsChecked.remove(i);
                notifyItemRemoved(i);
                i--;
            }
        }
    }

    public void updateData(List<I> currentData) {
        if (mData.size() != currentData.size()) {
            for (int i = 0; i < currentData.size(); i++) {
                if (mData.size() <= i) {
                    insertItem(i, currentData.get(i));
                } else {
                    if (!mData.get(i).toString().equals(currentData.get(i).toString())) {
                        insertItem(i, currentData.get(i));
                    }
                }
            }
        }
    }

    public void setOnRemoveListener(OnRemoveListener<I> onRemoveListener) {
        mOnRemoveListener = onRemoveListener;
    }

    @Override
    public void setData(List<I> data) {
        mAreItemsChecked = new ArrayList<>(data.size());
        for (int i = 0; i < data.size(); i++) {
            mAreItemsChecked.add(false);
        }
        super.setData(data);
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_ITEM) {
            view = mInflater.inflate(R.layout.card_view, parent, false);
            return new Holder(view);
        } else {
            view = mInflater.inflate(R.layout.recycler_progress_view, parent, false);
            return new ProgressViewHolder(view);
        }
    }

    public interface OnRemoveListener<T> {
        void onRemove(T item);
    }

    public class Holder extends AdapterWithLoadingData<I>.Holder {

        private CheckBox mCheckBox;

        private Holder(View itemView) {
            super(itemView);
            mParent.registerForContextMenu(itemView);
            itemView.setOnLongClickListener(view -> {
                setItemChecked(getAdapterPosition(), true);
                return false;
            });
            mCheckBox = itemView.findViewById(R.id.item_checkBox);
            mCheckBox.setOnClickListener(view ->
                    setItemChecked(getAdapterPosition(), mCheckBox.isChecked())
            );
        }

        @Override
        void bind(I item) {
            super.bind(item);
            if (mOnBindListener != null) {
                mOnBindListener.onBind(
                        item,
                        getAdapterPosition(),
                        mImageView,
                        mFirstTextField,
                        mSecondTextField
                );
            }
            if (mIsCheckingModeEnabled) {
                showCheckbox();
            } else {
                hideCheckbox();
            }
        }

        private void showCheckbox() {
            mCheckBox.setVisibility(View.VISIBLE);
            mCheckBox.setChecked(mAreItemsChecked.get(getAdapterPosition()));
            float[] properties = new float[]{0, 1};
            ObjectAnimator scaleUp = ObjectAnimator.ofPropertyValuesHolder(
                    mCheckBox,
                    PropertyValuesHolder.ofFloat("scaleX", properties),
                    PropertyValuesHolder.ofFloat("scaleY", properties)
            );
            scaleUp.setDuration(CHECKBOX_ANIMATION_DURATION);
            scaleUp.start();
        }

        private void hideCheckbox() {
            mCheckBox.setChecked(false);
            float[] properties = new float[]{1, 0};
            ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                    mCheckBox,
                    PropertyValuesHolder.ofFloat("scaleX", properties),
                    PropertyValuesHolder.ofFloat("scaleY", properties)
            );
            scaleDown.setDuration(CHECKBOX_ANIMATION_DURATION);
            scaleDown.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mCheckBox.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            scaleDown.start();
        }

        @Override
        public void onClick(View v) {
            if (mIsCheckingModeEnabled) {
                mCheckBox.toggle();
                setItemChecked(getAdapterPosition(), mCheckBox.isChecked());
            } else {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(mItem);
                }
            }
        }

    }

}
