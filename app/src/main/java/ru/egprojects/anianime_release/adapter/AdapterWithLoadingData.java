package ru.egprojects.anianime_release.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.egprojects.anianime_release.R;

public class AdapterWithLoadingData<I> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int VIEW_TYPE_ITEM = 1;
    static final int VIEW_TYPE_PROGRESS = 2;
    LayoutInflater mInflater;
    List<I> mData;
    OnClickListener<I> mOnClickListener;
    OnBindListener<I> mOnBindListener;
    private Context mContext;
    private int mPage;
    private boolean mHasNext, mNeedUpdate;
    private String mQuery;
    private OnLoadListener mOnLoadListener;
    private OnSearchListener mOnSearchListener;

    public AdapterWithLoadingData(Context context, LayoutInflater inflater) {
        mContext = context;
        mInflater = inflater;
        mPage = 1;
        mHasNext = true;
        mNeedUpdate = true;
    }

    public void search(String query) {
        mQuery = query;
        mData = null;
        mPage = 1;
        mHasNext = true;
        mNeedUpdate = true;
        if (mOnSearchListener != null) {
            mOnSearchListener.onSearch(mPage, mQuery);
        }
        notifyDataSetChanged();
    }

    public void cancelSearch() {
        if (mQuery == null) {
            return;
        }

        mQuery = null;
        mData = null;
        mPage = 1;
        mHasNext = true;
        mNeedUpdate = true;
        if (mOnLoadListener != null) {
            mOnLoadListener.onLoadData(mPage);
        }
        notifyDataSetChanged();
    }

    public synchronized void addData(final List<I> newData) {
        if (newData.size() == 0) {
            mHasNext = false;
            notifyItemChanged(getItemCount() - 1);
        } else {
            mHasNext = true;
            new Thread(() -> {
                List<I> data = new ArrayList<>(mData.size() + newData.size());
                data.addAll(mData);
                data.addAll(newData);
                int initialSize = mData.size();
                mData = data;
                for (int i = initialSize; i < mData.size(); i++) {
                    notifyItemInserted(i);
                }
            }).run();
            if (newData.size() < 5) {
                mPage++;
                if (mQuery == null) {
                    if (mOnLoadListener != null) {
                        mOnLoadListener.onLoadData(mPage);
                    }
                } else if (mOnSearchListener != null) {
                    mOnSearchListener.onSearch(mPage, mQuery);
                }
            }
        }
    }

    public void updateData(boolean showProgress) {
        if (showProgress) {
            mData = null;
            notifyDataSetChanged();
        }
        mPage = 1;
        mHasNext = true;
        mNeedUpdate = true;
        if (mOnLoadListener != null) {
            if (mQuery == null) {
                mOnLoadListener.onLoadData(mPage);
            } else {
                mOnSearchListener.onSearch(mPage, mQuery);
            }
        }
    }

    public List<I> getData() {
        return mData;
    }

    public void setData(List<I> data) {
        mNeedUpdate = false;
        if (mContext instanceof OnDataUpdatedListener) {
            ((OnDataUpdatedListener) mContext).onDataUpdated();
        }
        if (data.size() != 0) {
            mData = data;
            mHasNext = true;
        } else {
            mHasNext = false;
        }
        notifyDataSetChanged();
    }

    public boolean needUpdate() {
        return mNeedUpdate;
    }

    public void setOnLoadListener(OnLoadListener onLoadListener) {
        mOnLoadListener = onLoadListener;
        if (mOnLoadListener != null) {
            mOnLoadListener.onLoadData(mPage);
        }
    }

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        mOnSearchListener = onSearchListener;
    }

    public void setOnBindListener(OnBindListener<I> onBindListener) {
        mOnBindListener = onBindListener;
    }

    public void setOnClickListener(OnClickListener<I> onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_ITEM) {
            view = mInflater.inflate(R.layout.card_view, parent, false);
            return new Holder(view);
        } else {
            view = mInflater.inflate(R.layout.recycler_progress_view, parent, false);
            return new ProgressViewHolder(view);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == VIEW_TYPE_ITEM) {
            ((Holder) holder).bind(mData.get(position));
            if (getItemCount() - position == 5 || (getItemCount() < 5 && position == 0)) {
                mPage++;
                if (mQuery == null) {
                    if (mOnLoadListener != null) {
                        mOnLoadListener.onLoadData(mPage);
                    }
                } else if (mOnSearchListener != null) {
                    mOnSearchListener.onSearch(mPage, mQuery);
                }
            }
        } else {
            ((ProgressViewHolder) holder).bind();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mData == null || mData.size() == 0 || position == mData.size()) {
            return VIEW_TYPE_PROGRESS;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 1 : mData.size() + 1;
    }

    @FunctionalInterface
    public interface OnLoadListener {
        void onLoadData(int page);
    }

    @FunctionalInterface
    public interface OnSearchListener {
        void onSearch(int page, String query);
    }

    @FunctionalInterface
    public interface OnDataUpdatedListener {
        void onDataUpdated();
    }

    @FunctionalInterface
    public interface OnBindListener<I> {
        void onBind(I item, int pos, ImageView image, TextView firstText, TextView secondText);
    }

    @FunctionalInterface
    public interface OnClickListener<I> {
        void onClick(I item);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        I mItem;
        ImageView mImageView;
        TextView mFirstTextField;
        TextView mSecondTextField;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mImageView = itemView.findViewById(R.id.item_icon);
            mFirstTextField = itemView.findViewById(R.id.item_text1);
            mSecondTextField = itemView.findViewById(R.id.item_text2);
        }

        void bind(I item) {
            mItem = item;
            if (mOnBindListener != null) {
                mOnBindListener.onBind(
                        item,
                        getAdapterPosition(),
                        mImageView,
                        mFirstTextField,
                        mSecondTextField
                );
            }
        }

        @Override
        public void onClick(View v) {
            if (mOnClickListener != null) {
                mOnClickListener.onClick(mItem);
            }
        }

    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private TextView mMessageView;
        private ProgressBar mProgressBar;

        ProgressViewHolder(View view) {
            super(view);
            mLayout = view.findViewById(R.id.progress_bar_layout);
            mMessageView = view.findViewById(R.id.recycler_message);
            mProgressBar = view.findViewById(R.id.data_loading_progress_bar);
            mProgressBar.getIndeterminateDrawable().setColorFilter(
                    mContext.getResources().getColor(R.color.colorPrimary),
                    PorterDuff.Mode.SRC_ATOP
            );
        }

        private void bind() {
            if (mHasNext && mOnLoadListener != null) {
                if (mData == null || mData.size() == 0) {
                    mLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                    ));
                } else {
                    mLayout.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    ));
                }
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                mProgressBar.setVisibility(View.GONE);
                if (getItemCount() == 1) {
                    mMessageView.setText("Дата отсутствует");
                }
            }
        }

    }

}
